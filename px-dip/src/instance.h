#pragma once

#include <cstdint>
#include <memory>

class Instance {
public:
    Instance();
    Instance(const Instance&) = delete;
    Instance& operator=(const Instance&) = delete;
    ~Instance();
    void resize(uint32_t max_px_count);
    // getters
    const uint32_t get_current_max_px_count();
    uint16_t* get_pch();
    uint16_t* get_ptv();
    uint16_t* get_dst0();
    uint16_t* get_dst1();

private:
    class Priv;
    std::unique_ptr<Priv> p;
};