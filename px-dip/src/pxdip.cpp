#include "pxdip.h"
#include <cstdio>

#define STRINGIFY(s) #s
#define XSTR(s) STRINGIFY(s)
#define AUXVER XSTR(PERF4.2 - PXDIP_VER_AUX)

#define REVISION 7

unsigned int pxdip_api_version() {
    return PXDIP_API;
}

unsigned int pxdip_revision() {
    return REVISION;
}

const char* pxdip_version_desc() {
    return AUXVER;
}

void pxdip_setmask(const void* inst, const uint32_t count, uint32_t* const data, const uint32_t mask) {
    for (int i = 0; i < count; ++i) {
        uint32_t& v = data[i];
        v = v | mask;
    }
}